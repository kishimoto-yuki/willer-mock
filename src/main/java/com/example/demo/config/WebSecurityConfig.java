package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//	@Autowired
//	private DataSource dataSource;

	@Override
	public void configure(WebSecurity web) throws Exception {
		//CSS,JS,imagesへのアクセスは許可
		web.ignoring().antMatchers("/css/**", "/js/**", "/images/**");
	}

	@Override
	public void configure(final HttpSecurity http) throws Exception {

		http.authorizeRequests()
		.antMatchers("/login","/account").permitAll() //ログイン/アカウント作成画面は認証なし許可
//		.antMatchers("/h2-console/**").permitAll() //h2DBコンソール仕様設定
//		.anyRequest().authenticated() //それ以外はすべて認証された状態以外は拒否
		.and()
		.formLogin() // ログインページに飛ばす
		.loginProcessingUrl("/login") // ログイン処理をするURL
		.loginPage("/login") // ログインページのURL
		//	        .failureUrl("/login/error") // ログイン処理失敗時の遷移先
		.defaultSuccessUrl("/menu") // 認証成功時の遷移先
		.usernameParameter("email").passwordParameter("password")
		.and()
		.rememberMe()
		.rememberMeParameter("remember-me")//formのnameと紐づいている
		.key("secret-key")//この文字列で暗号化を行う
		.alwaysRemember(true)
		.rememberMeParameter("remember-me")//Cookie名
		.tokenValiditySeconds(24 * 60 * 60);//有効期限

		http.logout()
		.logoutRequestMatcher(new AntPathRequestMatcher("/logout**")) // ログアウト処理を起動させるパス
		.logoutSuccessUrl("/login") // ログアウト完了時のパス
		.deleteCookies("JSESSIONID").invalidateHttpSession(true).permitAll();
		
		//h2DBコンソール使用設定
		//		http.csrf().disable();
		//        http.headers().frameOptions().disable();
	}

	//パスワードのハッシュ化でBCryptを使うことができる。
//	@Bean
//	public PasswordEncoder passwordEncoder() {
//		return new BCryptPasswordEncoder();
//	}
//
//	@Bean
//	public PersistentTokenRepository tokenRepository() {
//		JdbcTokenRepositoryImpl jdbcTokenRepositoryImpl = new JdbcTokenRepositoryImpl();
//		jdbcTokenRepositoryImpl.setDataSource(this.dataSource);
//		return jdbcTokenRepositoryImpl;
//	}



}