package com.example.demo.app.menu;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("menu")
public class MenuController {
	
	@RequestMapping("")
	String index() {
		return "menu/index";
	}
	
}
