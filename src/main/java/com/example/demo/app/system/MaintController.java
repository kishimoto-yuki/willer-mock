package com.example.demo.app.system;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("system/maint")
public class MaintController {

    @RequestMapping("/search")
    String index(Model model) {
        model.addAttribute("type", "search");
        model.addAttribute("show",true);
        return "system/maint/search.html";
    }

    @RequestMapping("/result")
    String result(Model model) {
        model.addAttribute("type", "result");
        model.addAttribute("show",false);
        return "system/maint/search.html";
    }

    @RequestMapping("/edit")
    String edit(Model model) {
        model.addAttribute("type","edit");
        return "system/maint/form.html";
    }

    @RequestMapping("/regist")
    String regist(Model model) {
        model.addAttribute("type","regist");
        return "system/maint/form.html";
    }

}
