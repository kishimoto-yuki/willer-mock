package com.example.demo.app.system;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("system/maint/category")
public class MaintCategoryController {

	@RequestMapping("/search")
	String index(Model model) {
		model.addAttribute("type", "search");
		model.addAttribute("show",true);
		return "system/maintCategory/search.html";
	}

	@RequestMapping("/result")
	String result(Model model) {
		model.addAttribute("type", "result");
		model.addAttribute("show",false);
		return "system/maintCategory/search.html";
	}
	
	@RequestMapping("/edit")
	String edit(Model model) {
		model.addAttribute("type","edit");
		return "system/maintCategory/form.html";
	}

	@RequestMapping("/regist")
	String regist(Model model) {
		model.addAttribute("type","regist");
		return "system/maintCategory/form.html";
	}
}
