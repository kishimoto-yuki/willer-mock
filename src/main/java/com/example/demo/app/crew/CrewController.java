package com.example.demo.app.crew;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("crew")
public class CrewController {

	@RequestMapping("search")
	String index(Model model) {
		model.addAttribute("type", "search");
		model.addAttribute("show",true);
		return "crew/search";
	}
	
	@RequestMapping("result")
	String result(Model model) {
		model.addAttribute("type", "result");
		model.addAttribute("show",false);
		return "crew/search";
	}
	
	@RequestMapping("detail")
	String detail(Model model) {
		return "crew/detail";
	}
	
	@RequestMapping("edit")
	String edit(Model model) {
		model.addAttribute("type", "edit");
		return "crew/form";
	}
	
	@RequestMapping("regist")
	String regist(Model model) {
		model.addAttribute("type", "regist");
		return "crew/form";
	}
	
}
