package com.example.demo.app.car;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("car")
public class CarController {

    @RequestMapping("search")
    String index(Model model) {
        model.addAttribute("type", "search");
        model.addAttribute("show",true);
        return "car/search";
    }

    @RequestMapping("result")
    String result(Model model) {
        model.addAttribute("type", "result");
        model.addAttribute("show",false);
        return "car/search";
    }

    @RequestMapping("detail")
    String detail(Model model) {
        return "car/detail";
    }

    @RequestMapping("edit")
    String edit(Model model) {
        model.addAttribute("type", "edit");
        return "car/form";
    }

    @RequestMapping("regist")
    String regist(Model model) {
        model.addAttribute("type", "regist");
        return "car/form";
    }

    @RequestMapping("delete")
    String delete(Model model) {
        return "car/delete";
    }

    @RequestMapping("output/search")
    String outputIndex(Model model) {
        model.addAttribute("type", "search");
        model.addAttribute("show",true);
        return "car/output";
    }
    @RequestMapping("output/result")
    String outputResult(Model model) {
        model.addAttribute("type", "result");
        model.addAttribute("show",false);
        return "car/output";
    }
}
