package com.example.demo.app.accident;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("accident")
public class AccindentsController {

	@RequestMapping("search")
	String index(Model model) {
		model.addAttribute("type", "search");
		model.addAttribute("show", true);
		return "accident/search";
	}
	
	@RequestMapping("result")
	String result(Model model) {
		model.addAttribute("type", "result");
		model.addAttribute("show", false);
		return "accident/search";
	}

	@RequestMapping("detail")
	String detail(Model model) {
		return "accident/detail";
	}
	
	@RequestMapping("edit")
	String edit(Model model) {
		model.addAttribute("type", "edit");
		return "accident/form";
	}
	
	@RequestMapping("regist")
	String regist(Model model) {
		model.addAttribute("type", "regist");
		return "accident/form";
	}
}
