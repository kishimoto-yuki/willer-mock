package com.example.demo.app.system_login;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("system/login")
public class SystemLoginController {
	
	@RequestMapping("search")
	String index(Model model) {
		model.addAttribute("type", "search");
		model.addAttribute("show",true);
		return "system_login/search";
	}
	
	@RequestMapping("result")
	String result(Model model) {
		model.addAttribute("type", "result");
		model.addAttribute("show",false);
		return "system_login/search";
	}

	@RequestMapping("/edit")
	String edit(Model model) {
		model.addAttribute("type","edit");
		return "system_login/form.html";
	}

	@RequestMapping("/regist")
	String regist(Model model) {
		model.addAttribute("type","regist");
		return "system_login/form.html";
	}
	
}
