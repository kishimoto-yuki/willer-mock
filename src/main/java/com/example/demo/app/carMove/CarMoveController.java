package com.example.demo.app.carMove;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("car/move")
public class CarMoveController {
	
    @RequestMapping("search")
    String index(Model model) {
        model.addAttribute("type", "search");
        model.addAttribute("show",true);
        return "carMove/search";
    }

    @RequestMapping("result")
    String result(Model model) {
        model.addAttribute("type", "result");
        model.addAttribute("show",false);
        return "carMove/search";
    }

    @RequestMapping("detail")
    String detail(Model model) {
        return "carMove/detail";
    }

    @RequestMapping("edit")
    String edit(Model model) {
        model.addAttribute("type", "edit");
        return "carMove/form";
    }

    @RequestMapping("regist")
    String regist(Model model) {
        model.addAttribute("type", "regist");
        return "carMove/form";
    }

    @RequestMapping("delete")
    String delete(Model model) {
        return "carMove/delete";
    }
    
}
