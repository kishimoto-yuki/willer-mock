package com.example.demo.app.education;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("education")
public class EducationController {
	
	@RequestMapping("search")
	String index(Model model) {
		model.addAttribute("type", "search");
		model.addAttribute("show",true);
		return "education/search";
	}
	
	@RequestMapping("result")
	String result(Model model) {
		model.addAttribute("type", "result");
		model.addAttribute("show",false);
		return "education/search";
	}
	
	@RequestMapping("detail")
	String detail(Model model) {
		return "education/detail";
	}
	
	@RequestMapping("edit")
	String edit(Model model) {
		model.addAttribute("type", "edit");
		return "education/form";
	}
	
	@RequestMapping("regist")
	String regist(Model model) {
		model.addAttribute("type", "regist");
		return "education/form";
	}
	
}
