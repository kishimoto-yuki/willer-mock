package com.example.demo.app.maintenance;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("maintenance")
public class MaintenanceController {
	
	@RequestMapping("search")
	String index(Model model) {
		model.addAttribute("type", "search");
		model.addAttribute("show",true);
		return "maintenance/search";
	}
	
	@RequestMapping("result")
	String result(Model model) {
		model.addAttribute("type", "result");
		model.addAttribute("show",false);
		return "maintenance/search";
	}
	
	@RequestMapping("detail")
	String detail(Model model) {
		return "maintenance/detail";
	}
	
	@RequestMapping("edit")
	String edit(Model model) {
		model.addAttribute("type", "edit");
		return "maintenance/form";
	}
	
	@RequestMapping("regist")
	String regist(Model model) {
		model.addAttribute("type", "regist");
		return "maintenance/form";
	}
	
}
