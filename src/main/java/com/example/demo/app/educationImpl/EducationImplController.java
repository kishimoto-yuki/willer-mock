package com.example.demo.app.educationImpl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("educationImpl")
public class EducationImplController {
	
	@RequestMapping("search")
	String index(Model model) {
		model.addAttribute("type", "search");
		model.addAttribute("show",true);
		return "educationImpl/search";
	}
	
	@RequestMapping("result")
	String result(Model model) {
		model.addAttribute("type", "result");
		model.addAttribute("show",false);
		return "educationImpl/search";
	}
	
	@RequestMapping("detail")
	String detail(Model model) {
		return "educationImpl/detail";
	}
	
	@RequestMapping("edit")
	String edit(Model model) {
		model.addAttribute("type", "edit");
		return "educationImpl/form";
	}
	
}
