
$(function(){
	
	// 選任解任歴の入力欄表示を切替え
//	$('.mechanicDisp').hide();
//	$('.operationMngDisp').hide();
//	
//	$('#occupationType').on('change', function(){
//		switch($(this).prop('selectedIndex')) {
//			case 1:
//				$('.mechanicDisp').show();
//				$('.crewDisp').hide();
//				$('.operationMngDisp').hide();
//				break;
//			case 2:
//				$('.operationMngDisp').show();
//				$('.crewDisp').hide();
//				$('.mechanicDisp').hide();
//				break;
//			default:
//				$('.crewDisp').show();
//				$('.mechanicDisp').hide();
//				$('.operationMngDisp').hide();
//				break;
//		}
//	});

	// 整備管理者・整備管理補助者の入力欄表示を切替え
	$('.maintSub').hide();
	
	$('#maintAdmin').on('change', function(){
		if($(this).prop('selectedIndex') === 0) {
			$('.maintMain').show();
			$('.maintSub').hide();
		} else {
			$('.maintMain').hide();
			$('.maintSub').show();
		}
	});

	// 運行管理者・運行管理補助者の入力欄表示を切替え
	$('.operationMngSub').hide();
	
	$('#operationMngAdmin').on('change', function(){
		if($(this).prop('selectedIndex') === 0) {
			$('.operationMngMain').show();
			$('.operationMngSub').hide();
		} else {
			$('.operationMngMain').hide();
			$('.operationMngSub').show();
		}
	});

	// 生年月日を入力すると年齢が自動計算で表示される
	$('.dateOfBirth').on('blur', function(){
		let today = new Date();
		let tdate = (today.getFullYear()*10000 ) + ((today.getMonth()+1)*100 ) + today.getDate();
		let birth = $(this).val().replace(/-/g, '');
		$('.calculatedAge').text(Math.floor((tdate - birth)/10000));
	});
});