$(function(){
	
	var columnNum = $('#dt')[0].rows[0].cells.length-1
	
  // datatableの設定を変更
  $("#dt").DataTable({
      searching: false,       // 検索非表示
      info: false,            // 件数情報非表示
      paging: false,          // ページング非表示
      lengthChange: false,    // 件数切り替えプルダウン非表示
      columnDefs: [           // targetカラム番号をソートさせない
        {
          orderable: false,
          targets: columnNum
        }
      ],
      aaSorting: [],
  });
	
});