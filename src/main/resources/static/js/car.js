
$(function(){
	
	// 車両管理登録画面
	// 「運行開始準備」表示後、折りたたみボタンを無効化
	$('#operationPreparation').on('show.bs.collapse', function() {
		$(this).prop('disabled', true);
	});

	// 車両管理削除画面
	// 「車両売却タスク設定」表示後、折りたたみボタンを無効化
	$('#saleTask').on('show.bs.collapse', function() {
		$(this).prop('disabled', true);
	});

	// 車両移動管理登録画面
	// 「運行開始準備」表示後、折りたたみボタンを無効化
	$('#carMoveTask').on('show.bs.collapse', function() {
		$(this).prop('disabled', true);
	});
});