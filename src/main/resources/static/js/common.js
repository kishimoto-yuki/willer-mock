
$(function(){
	
  $('.checkAll').on('change', function() {
    $('.' + this.id).prop('checked', this.checked);
  });

	jQuery(document).on('keyup', '.input_numeric', function(e){
	  if(e.keyCode === 9 || e.keyCode === 16) return;
	  this.value = this.value.replace(/[^0-9]+$/,'');
	});
	 
	jQuery(document).on('blur', '.input_numeric',function(){
	  this.value = this.value.replace(/[^0-9]+$/,'');
	});
  
  	jQuery(document).on('keyup', '.input_alphanumeric', function(e){
	  if(e.keyCode === 9 || e.keyCode === 16) return;
	  this.value = this.value.replace(/[^0-9a-zA-Z]+/i,'');
	});
	 
	jQuery(document).on('blur', '.input_alphanumeric',function(){
	  this.value = this.value.replace(/[^0-9a-zA-Z]+/i,'');
	});
	
	
	$('.numberMaxLength8').keyup(function(){
		
		var str = $(this).val();
		
		if (str.length > 8) {
			$(this).val(str.slice(0,8));
		}
		
	});
	
	$('.numberMaxLength5').keyup(function(){
		
		var str = $(this).val();
		
		if (str.length > 5) {
			$(this).val(str.slice(0,5));
		}
		
	});
	
	$('.numberMaxLength4').keyup(function(){
		
		var str = $(this).val();
		
		if (str.length > 4) {
			$(this).val(str.slice(0,4));
		}
		
	});
	
	$('.numberMaxLength3').keyup(function(){
		
		var str = $(this).val();
		
		if (str.length > 3) {
			$(this).val(str.slice(0,3));
		}
		
	});
	
	//	<!-- 入力必須 -->
	$(function(){
	   $('.required').on('keydown keyup keypress change focus blur', function(){
	       if($(this).val() == ''){
	           $(this).css({backgroundColor:'#ffcccc'});
	       } else {
	           $(this).css({backgroundColor:'#fff'});
	       }
	   }).change();
	});
});
