
$(function(){
	//実施日一括設定
	$("#day").click(function() {
		if ($(this).prop("checked") == true) {
			$('#day-2').attr('readonly',true);
			$('#day-3').attr('readonly',true);
	    } else {
	        $('#day-2').attr('readonly',false);
			$('#day-3').attr('readonly',false);
	    }
	});
	//実施日一括設定時反映
    $("#day-1").change(function(){
    	if($("#day").prop("checked") == true) {
    		$('#day-2').val($(this).val());
			$('#day-3').val($(this).val());
    	}
    });
    
    //実施状況一括設定時反映
    $('input[name=radio1]').change(function(){
    	if($("#state").prop("checked") == true) {
    		$('[name=radio2][value="'+$(this).val()+'"]').prop('checked',true);
    		$('[name=radio3][value="'+$(this).val()+'"]').prop('checked',true);
    	}
    });
    
    //教育訓練結果一括設定
    $("#result").click(function() {
		if ($(this).prop("checked") == true) {
			$('#result-2').attr('readonly',true);
			$('#result-3').attr('readonly',true);
	    } else {
	        $('#result-2').attr('readonly',false);
			$('#result-3').attr('readonly',false);
	    }
	});
    
    //教育訓練結果一括設定時反映
    $("#result-1").change(function(){
    	if($("#result").prop("checked") == true) {
    		$('#result-2').val($(this).val());
			$('#result-3').val($(this).val());
    	}
    });
    
    
	
});