$(function(){
		
	$('#maintStandard1').on('change', function(){
		if($(this).prop('checked')) {
			$('#maintStandardValue1').prop('disabled', false);
		} else {
			$('#maintStandardValue1').prop('disabled', true);
		}
	});
	$('#maintStandard2').on('change', function(){
		if($(this).prop('checked')) {
			$('#maintStandardValue2').prop('disabled', false);
		} else {
			$('#maintStandardValue2').prop('disabled', true);
		}
	});
});